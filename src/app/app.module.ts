import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodoComponent } from './todo/todo.component';
import { RegistrationComponent } from './registration/registration.component';
import { TodosComponent } from './todos/todos.component';
import { LoginComponent } from './login/login.component';
import { CodesComponent } from './codes/codes.component';
import { Routes, RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodoComponent,
    RegistrationComponent,
    TodosComponent,
    LoginComponent,
    CodesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path:'',component:TodosComponent},
      {path:'regiter',component:RegistrationComponent},
      {path:'login',component:LoginComponent},
      {path:'codes',component:CodesComponent},
      {path:'**',component:TodosComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
