import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos = [{"text":"Study json", "id":1},
          {"text":"Do final project","id":2}, 
          {"text":"Do homework","id":3}];

  constructor() { }

  ngOnInit() {
  }

}
